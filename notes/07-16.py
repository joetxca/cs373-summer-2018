# -----------
# Mon, 16 Jul
# -----------

map(f, a) # map captures the object that a is a pointer or reference to
a = ...   # would not affect the map
vs
a += ...  # would affect the map

# Java

class Mammal {
    ...}

class Tiger extends Mammal {
    ...}

class Test {
    public static f (Tiger y) {
        ...}

    public static g (Mammal y) {
        ...}

    public static main (...) {
        f(new Tiger(...));

        f(new Mammal(...)); # no

        g(new Mammal(...));

        g(new Tiger(...));

def f (*t, **d) :
    ...

"""
three tokens:
*: zero or more
+: one  or more
?: zero or one
"""

"""
(.*) (.*)
\2 \1     # switch first name and last name

[abc] # a or b or c
[a-z] # a through z
[^a]  # not a
"""

"""
relational algebra
"""

"""
algebras
    sets of elements
    sets of operations
"""

"""
integer arithmetic
    integers
    +, *, /, -
    closed on +, -, *
    open   on /
"""

"""
closure property on sets
"""

"""
integers are closed on addition
"""

"""
relational algebra
    tables, relations
    select, project, several flavors of join
    closed on all operations
"""

"""
movie table

name year director genre
shane 1954 "george stevens" western
"star wars" 1977 "george lucas" western
"""

"""
select(movie, unary predicate)
=> a table with a subset of the rows
"""

"""
project(movie, attr1, attr2, attr3, ...)
=> a table with a subset of the cols
"""

"""
a Python data structure to represent a table
list of dicts
"""

[
{"name":"shane", "year":1953, "director":"george stevens", "genre":"western"},
["nane":"star wars", "year":1977, "director":"george lucas", "genre":"western"]
]

def select (a, up) :
    for v in a :
        if up(v) :
            yield v

def select (a, up) :
    return (v for v in a if up(v))

def select (a, up) :
    return filter(up, a)
