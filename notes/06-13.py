# -----------
# Wed, 13 Jun
# -----------

(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
n + n/2 + 1
n + n>>1 + 1

"""
REPL: read eval print loop
"""

"""
this is an lazy cache
let's build a cache
let's try an list
let's try a million

let's a dict instead of a list
"""

"""
what would an eager cache
compute everything before you read anything
"""

"""
what would be a meta cache
precompute outside of the oracle

cache the mcl of 1 to 1000
1000 to 2000
2000 to 3000
3000 to 4000
...

what is the mcl of 1500 to 3500

compute the mcl of 1500 to 2000
look up the mcl of 2000 to 3000
compute the mcl of 3000 to 3500
"""

"""
architect the code so that it's easy to test

kernel
    Collatz.py

run harness
    RunCollatz.py

test harness
    TestCollat.py
"""
