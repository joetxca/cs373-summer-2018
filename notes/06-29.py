# -----------
# Wed, 29 Jun
# -----------

"""
put out a review sheet for T1 later today

10 multiple-choice, 5 pts, 50
4 coding questions, 25 pts (5 tests of 5 pts), 100
total: 150 pts, 15% of the final term

iterator of an iterator
yield
"""

"""
if we wanted a class Foo to be iterable:

define __iter__
return a FooIterator, which I would have to write

where I would have to define __next__()
__next__() eventually raises a StopIteration exception
it also has to have an __iter__() that returns itself
"""

x = Foo(...)

for v in x :
    ...

a = [2, 3, 4]
print(type(a)) # list

p = iter(a)
print(type(p)) # list iterator

print(a is p)  # False

q = iter(p)
print(type(q)) # list iterator

print(p is q)  # True

# invoke a function f on every element of a container

for v in a:
    f(v)

# invoke a function f on the first element and the function g on the rest of the elements of a container

p = iter(a)
f(next(p))

for v in p :
    g(v)

print(next(p)) # StopIteration

"""
indexables: list, tuple, str
non-idexables: set, frozenset, dict
"""

list(?) # ? would have to iterable

for v in a :
    print(v)

p = iter(a)
try :
    while True :
        v = next(p)
        print(v)
except StopIteration :
    pass

# yield

def f () :
    print("abc")
    yield 2
    print("def")
    yield 3
    print("ghi")

p = f()       # <nothing>

q = iter(p)
print(p is q) # True

v = next(p)   # abc
print(v)      # 2

v = next(p)   # def
print(v)      # 3

v = next(p)   # ghi, raises StopIteration

"""
summary
"""

"""
containers and iterators are both iterables
"""

"""
containers: list, tuple, str, set, frozenset, dict, range
have __iter__, which returns a different type of object, a type of iterator
do NOT have __next__
can be iterated over repeatedly, without exahaustion
"""

for v in a :
    print(v)

for v in a :
    print(v)

"""
iterators: list_iterator, tuple_iterator, str_iterator, set_iterator, frozenset_iterator, dict_iterator, range_iterator
have __iter__, which returns itself
do have __next__
can be iterated over ONLY once
"""

p = iter(a)

for v in p :
    print(v)

for v in p : # nothing, already exhausted
    print(v)
