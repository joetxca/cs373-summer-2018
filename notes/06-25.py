# -----------
# Mon, 25 Jun
# -----------

x == y # x.__eq__(y)

"""
astronomy

stars
asteroids
planets
comets
"""

i = (2, 3, 4)
j = (2, 3, 4)
print(i is j) # false

"""
list's += will take an ITERABLE on the right hand side
list's +  only takes another list
"""

s = "abc"
t = "abc"
print(s is t) # true

i = 2
j = 3
i += j # i = i + j

"""
x += y is the same as x = x + y
when? for the immutables
"""

"""
tuple's += only takes another tuple
tuple's +  only takes another tuple
"""
