# -----------
# Mon, 18 Jun
# -----------

"""
type
operators
"""

"""
list is heterogenous
order matters
mutable: content or length
front-loaded array
cost of adding an element
to the beginning: O(n)
to the middle: O(n)
to the back: amortize O(1)
"""
[2, 3] == [3, 2] # false

"""
tuple is heterogenous
order matters
immutable
can't change the content, can't change length
"""
a = [2, 3]
b = (4, a, 5)
print(b)     # (4, [2, 3], 5)
a += [6]
print(a)     # [2, 3, 6]
print(b)     # (4, [2, 3, 6], 5)
             # implicitly changed the tuple

list1 += list2

"""
set is heterogenous
order doesn't matter
mutable: length
how is set implemented? hash
elements have to be hashable
by default, what's hashable? are the immutables (str, tuple, frozenset)
mutables are: list, set, dict
cost of adding an element: O(1)
elements have to be unique
"""
{2, 3} == {3, 2} # true

set1 += set2 # fails
set1 |= set2 # might not change size

"""
dict is heterogenous
order doesn't matter
mutable: length, value, not the key
how is set implemented? hash
keys have to be hashable
by default, what's hashable? are the immutables (str, tuple, frozenset)
mutables are: list, set, dict
cost of adding an key/value pair: O(1)
keys have to be unique
"""

"""
deque
doubly-linked list
deque is heterogenous
mutable: content and length
cost of adding an element: O(1)
"""

x == y # x.__eq__(y)

# Python

i = 2
j = 3
k = (i + j) # addition

i + j # no good, an expression, must be part of something bigger

i = j # statement, can standalone

i += j # statement

# statements do NOT act like expressions

# Java

int i = 2;
int j = 3;
int k = (i + j);

k = (i + j);

i += j;

k = (i += j); # (i += j) is both an expression and a statement in C, C++, and Java, but not Python

k = ++i; # C, C++, and Java, but not Python
k = i++;

(i += j) = k # C++, but not C, Java, or Python
