# -----------
# Fri, 15 Jun
# -----------

mcl(10, 100)

b = 10
e = 100

m = (100 / 2) + 1 = 51

mcl(10, 100) = mcl(51, 100)

"""
aren't we throwing out 10 to 50
"""

mcl(200, 300)

b = 200
e = 300

m = 151

"""
pretend we don't have exceptions
"""

# make use of the return

def f (...) :
    ...
    if (...) :
        # <something wrong>
        return -1
    ...
    return ...

def g (...) :
    i = f(...)
    if (i == -1)
        # <something wrong>
...
g(...)
...

# make use of a global

e = 0

def f (...) :
    global e
    ...
    if (...) :
        # <something wrong>
        e = -1
        return ...
    ...
    return ...

def g (...) :
    global e
    e = 0
    i = f(...)
    if (e == -1)
        # <something wrong>
...
g(...)
...

# make use of a parameter

def f (..., e2) :
    ...
    if (...) :
        # <something wrong>
        e2[0] = -1
        return ...
    ...
    return ...

def g (...) :
    e = [0]
    i = f(..., e)
    if (e[0] == -1)
        # <something wrong>
...
g(...)
...

class T {
    int f (..., int[] e2) {
        ...
        if (???)
            e2[0] = -1
        ...
        return ...

    void g (...) {
        int[] e = {0};
        int i = f(..., e);
        if (e[0] == -1)
            # something wrong

# make use of exceptions

class E :
    ...

class E2 (E) :
    ...

def f (...) :
    ...
    if (...) : # something is wrong
        raise X(...)
    ...
    return ...

def g (...) :
    ...
    try :
        ...
        i = f(...)
        ...
    except E2 as e :
        # something else wrong, handle child first
    except E as e :
        # something wrong
    else :
        # only runs when no raise
    finally :
        # always
    ...
...
g(...)
...

# Java

class A { # creating an instance of class class that describe class A
    ...}

class T { # creating an instance of class class that describe class T
    Class c = A.class;

    A x = new A(...);
    Class c2 = x.getClass();


    Class c3 = Class.forName("A");

    s.o.p(c == c2) # true
    s.o.p(c == c3) # true

# Python

i = 2
print(type(i)) # int

print(type(int)) # type

print(type(type)) # type

# Java

class A {
    int i;}

class T {
    public void static main (...) {
        A x = new A(...);
        s.o.p(x.i);

# Python

a = [2, 3, 4]
print(type(a)) # list
print(len(a))  # 3

a = [2]
print(type(a)) # list
print(len(a))  # 1

a = []
print(type(a)) # list
print(len(a))  # 0

a = (2, 3, 4)
print(type(a)) # tuple
print(len(a))  # 3

a = (2)
print(type(a)) # int
print(len(a))  # error

a = (2,)
print(type(a)) # tuple
print(len(a))  # 1

a = ()
print(type(a)) # tuple
print(len(a))  # 0

a = [2, 3, 4]
b = [2, 3, 4]
print(a == b) # true,  content check
print(a is b) # false, identity check

# Java

int[] a = {2, 3, 4};
int[] b = {2, 3, 4};
s.o.p(a == b);             # false, identity check
s.o.p(a.equals(b))         # false
s.o.p(Arrays.equals(a, b)) # true,  content  check
