# -----------
# Mon, 23 Jul
# -----------

int i = 2;
int j = ++i;

int i = 2;
int j = i++;

f(i++);

int i = 2;
++i;

int i = 2;
i++;

"""
class relationships
    inheritance
    containment
"""

"""
interface List
    class AbstractList implements List
        class ArrayList extends AbstractList
"""

"""
class Vector
    class Stack extends Vector
"""
