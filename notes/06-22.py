# -----------
# Fri, 22 Jun
# -----------

"""
Miriam Grobman
    UTCS grad, '06
    Deutsche Bank, London, New York
    Wharton, MBA
    mining industry, Brazil
    consultant for women leadership

Nalini Belaramani
    Hong Kong, undergrad
    Motorola
    UTCS PhD grad, '09
    distributed filesytems
    Google engineer, tech lead, manager

Shan Wang
    UTCS grad, '06
    Stanford, PhD dropout
    Google engineer
"""
