# -----------
# Fri, 20 Jul
# -----------

"""
movie table

name year director genre
"shane" 1954 "george stevens" "western"
"star wars" 1977 "george lucas" "sci-fi"

movie table (director_id is a foreign key)

name year director_id genre
"shane" 1954 1 "western"
"star wars" 1977 2 "sci-fi"

director table

director_id name
1           "george stevens"
2           "george lucas"
"""

"""
in SQL the RA select and project are jointly with an SQL select
"""

"""
Python, JavaScript, C, Java are all procedural languages
SQL is a declarative language
"""
