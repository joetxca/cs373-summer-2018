# ----------
# Fri, 8 Jun
# ----------

"""
meet at least two people:
    name
    e-mail
    why are you taking the course
    what are your expectations
    google "cs373 spring 2018"
"""

"""
ACM  is around 80 years old
ICPC is around 40 years old
bi-weekly contest in GDD 1.304, Fri 5:30-8pm
we pick top 12 students, 4 groups of 3, regional competition

2014: A&M,    2nd, Rice, Morocco
2015: Baylor, 2nd, all 4 teams in the top 7, Rice, Thailand
2016: Baylor, 1, 2, 3, 4, South Dakota
2017: Baylor, 1, Beijing
"""

"""
Docker
"""

def f (n: int) :
    ...

"""
Collatz Conjecture
about 100 years old

take a pos int
if even, divide by 2
otherwise, multiply by 3 and add 1
repeat until 1

5 16 8 4 2 1

cycle length of 5 is 6
cycle length of 10 is 7

cycle length of 1 is 1

/  is true  division, output is always float
// is floor division, output is the type of the input

assertions are good for preconditions, postconditions
not good for testing, becauase the first failure stops the script

assertions are good for programmer error
not good for user errors
"""
