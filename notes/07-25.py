# -----------
# Wed, 25 Jul
# -----------

"""
Replace Type Code with State / Strategy

movie remains concrete
create price and it's children
    childrensprice
    newprice
    regularprice
the switch effectively moves to the constructor, therefore runs much less often
test harness does not change
remains easy to change movie type
"""

"""
Replace Type Code with Subclasses

movie becomes abstract
create subclasses of movie
    childrensmovie
    newmovie
    regularmovie
the switch is removed completely
test harness would have to change
becomes hard to change type of movie
"""

class A {
    public void f (long) {
        ...}}

class B extends A {
    @override
    public void f (int) {
        ...}

class T {
    public static void main (...) {
        A x = new B(...);
        x.f(2);           // B.f

class A {
    public void f (int) {...}       # most precarious
    public final void g (int) {...}
    public abstract void h (int);}

"""
children CAN override f()
children can NOT override g()
children MUST override h() or become abstract
"""

class A ...
class B extends A ...
class C extends A ...

class T {
    public static void main (...) {
        A x;
        if (...)
            x = new B(...);
        else
            x = new C(...);
        x.f(...);           # dynamic binding

"""
four cases where method calls are not dynamically bound in Java
    final class
    final method
    static method
    private method
"""
