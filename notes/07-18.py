# -----------
# Wed, 18 Jul
# -----------

def select (a, up) :
    ...

def project (r, *a) :
    for d in r :
        do = {}
        for k in a :
            if k in d :
                do[k] = d[k]
        yield do

def project (r, *a) :
    for d in r :
        yield {k : d[k] for k in a if k in d}

def project (r, *a) :
    return ({k : d[k] for k in a if k in d} for d in r)

"""
cross join
it takes two relations
produces a relation with the union of the rows from the two relations
"""

def cross_join (r, s) :
    for u in r :
        for v in s :
            yield {**u, **v}

def cross_join (r, s) :
    return ({**u, **v} for u in r for v in s)

"""
theta join
it takes two relations and a predicate
produces a relation with the union of the rows from the two relations that satisfy the predicate
conceptually, a cross join, followed by a select
"""

def theta_join (r, s, bp) :
    for u in r :
        for v in s :
            if bp(u, v) :
                yield {**u, **v}

def theta_join (r, s, bp) :
    return ({**u, **v} for u in r for v in s if bp(u, v))

"""
natural join
it takes two relations
produces a relation with the union of the rows from the two relations more matching attributes
conceptually
"""

def natural_join (r, s) :
    def bp (u, v) :
        for k in u :
            if k in v and u[k] != v[k]
                return False
        return True

    def bp (u, v) :
        return all(u[k] == v[k]) for k in u if k in v

    for u in r :
        for v in s :
            if bp(u, v) :
                yield {**u, **v}

"""
look up all()
all() has a companion any()
"""
