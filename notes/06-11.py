# -----------
# Mon, 11 Jun
# -----------

"""
attendance in Canvas you can ignore
make URLs clickable in Piazza
"""

class A {
    void f (...) {
        // this is a reference to the object that f was invoked on

    static void g (...) {
        // no this

A x = new A(...);
x.f(...); // this is a reference to x

A y = new A(...);
y.f(...); // this is a reference to y

class A :
    def f (self, ...) :
        # self is a reference to the object that f was invoked on

    def f2 (foo, ...) :
        # foo is a reference to the object that f was invoked on

    def f3 () : # doesn't work
        ...

    @staticmethod
    def g (...) :
        # no self

    @staticmethod
    def g2 () :
        ...

x = A()
x.f() # self is a reference to x

y = A()
y.f() # self is a reference to y

def h (...) :
    ...

def f (g, x) :
    return g(g(x))

def add1 (x) :
    return x + 1

print(f(add1, 2)) # 4

print(f(lambda (x) : x + 1, 2)) # 4

"""
1. run it as is, confirm  success


3. fix the code, confirm success
"""
