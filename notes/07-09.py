# -----------
# Mon,  9 Jul
# -----------

for v in a : # a has to be iterable
    ...

a = [[2, 3], [3, 4], {4, 5}]

for v, w in a : # a has to be iterable over iterables of length 2
    ...

for v, w, x in a : # a has to be iterable over iterables of length 3
    ...

x = range(2, 3)
print(x[1])     # print(x.__getitem__(1))

class range_iterator () :
    def __init__ (self, b, e) :
        self.b = b
        self.e = e

    def __iter__ (self) :
        return self

    def __next__ (self) :
        if self.b == self.e :
            raise StopIteration()
        v = self.b
        self.b += 1
        return v

class range :
    def __init__ (self, b, e) :
        self.b = b
        self.e = e

    def __iter__ (self) :
        return range_iterator(self.b, self.e)

    def __getitem__ (self, i) :
        v = self.b + i
        if v >= e :
            raise IndexError
        return v

class range :
    def __init__ (self, b, e) :
        self.b = b
        self.e = e

    def __iter__ (self) :
        b = self.b
        while b < self.e :
            yield b
            b += 1

    def __getitem__ (self, i) :
        v = self.b + i
        if v >= e :
            raise IndexError
        return v

"""
comprehensions with [] are eager and produce lists
comprehensions with {} are eager and produce sets or dicts

comprehensions with () are lazy and produce generators
"""
