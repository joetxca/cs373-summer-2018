# -----------
# Wed, 11 Jul
# -----------

"""
Python's     map is lazy,  is a generator,         is exhaustible
    needs __init__
    needs __iter__
    needs __next__
JavaScript's map is eager, produces another array, is not exhaustible
"""

class map :
    def __init__ (self, uf, a) :
        self.uf = uf
        self.p  = iter(a)

    def __iter__ (self) :
        return self

    def __next__ (self) :
        return self.uf(next(self.p))

def map (uf, a) :
    for v in a :
        yield uf(v)

def map (uf, a) :
    return (uf(v) for v in a)

def f (n) :
    return n + 1

print(type(f)) # function

print(f(2))    # 3

class g :
    def __call__ (self, n) :
        return n + 1

print(type(g)) # type

x = g()

print(type(x)) # g

print(x(2))    # 3
print(x.__call__(2)) # 3

"""
three tokens
=, *, **

two contexts
a function definiton
a function call
"""

def timeit (s1, s2, *, number) :
    ...
