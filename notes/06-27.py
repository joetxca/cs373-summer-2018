# -----------
# Wed, 27 Jun
# -----------

for v in a :
    ...

"""
what does a have to be?
iterable: range, list, tuple, str, set, dict
"""

a = [2, 3, 4]
print(type(a)) # list

p = iter(a)    # a.__iter__()
print(type(p)  # list iterator
print(a is p)  # false

try:
    while (True) :
        print(next(p)) # p.__next__()
except StopIteration:
    pass

# Java

for (int v: a) {
    ...

"""
what does a have to be?
iterable: TreeSet, MapSet, arrays, ArrayList, ...
"""

ArrayList x = new ArrayList();
Iterator  p = x.iterator();
while (p.hasNext())
    s.o.p(p.next());

"""
reduce takes three arguments:
    1. a binary function
    2. an iterable
    3. a value
"""

def reduce (bf, a, v) :
    for (i in range(len(a)) :
        v = bf(v, a[i])
    return v

def reduce (bf, a, v) :
    for (i in a) :
        v = bf(v, i)
    return v
